<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('App')->group(function() {
    Route::get('/', 'Http\Controllers\Site\HomeController')->name('site.home');

    Route::get('produtos', 'Http\Controllers\Site\CategoryController@index')->name('site.products');

    Route::get('produtos/{category:slug}', 'Http\Controllers\Site\CategoryController@show')->name('site.products.category');

    Route::get('blog', 'Http\Controllers\Site\BlogController')->name('site.blog');

    Route::view('sobre', 'site.about.index')->name('site.about');

    Route::get('contato', 'Http\Controllers\Site\ContactController@index')->name('site.contact');

    Route::post('contato', 'Http\Controllers\Site\ContactController@form')->name('site.contact.form');

});